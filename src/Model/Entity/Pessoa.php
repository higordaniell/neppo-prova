<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pessoa Entity
 *
 * @property int $id
 * @property string $identificao
 * @property string $nome
 * @property \Cake\I18n\FrozenDate $dataNascimento
 * @property int $sexo
 * @property string $endereco
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Pessoa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'identificao' => true,
        'nome' => true,
        'dataNascimento' => true,
        'sexo' => true,
        'endereco' => true,
        'created' => true,
        'modified' => true
    ];

    public $_sexos = [
        'Não declarado',
        'Masculino',
        'Feminino'
    ];

    protected function _getSexoEscrito() {
        $val = $this->_properties['sexo'];
        if($val >= 0 && $val <= 2)
            return $this->_sexos[ $val ];
        else
            return $this->_sexos[ 0 ];
    }
}

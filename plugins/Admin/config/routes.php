<?php
use Cake\Routing\Router;

Router::plugin(
    'Admin',
    ['path' => '/admin'],
    function ($routes) {
        $routes->connect('/', [
            'controller' => 'Pessoas',
            'action' => 'index'
        ]);
        $routes->connect('/:controller/', ['action' => 'index']);
        $routes->connect('/:controller/:action/*');
    }
);

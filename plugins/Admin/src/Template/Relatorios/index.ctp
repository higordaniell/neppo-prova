<section>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafico de sexo</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="sexoChart" style="height:250px"></canvas>

                    <p>
                        <b>Pessoas do sexo masculino: </b> <?= $sexoChart['masculino'] ?><br>
                        <b>Pessoas do sexo feminino: </b> <?= $sexoChart['feminino'] ?><br>
                        <b>Pessoas sexo indefinido: </b> <?= $sexoChart['indefinido'] ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="box box-warning ">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafico de idade</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="idadeChart" style="height:250px"></canvas>

                    <p>
                        <b>Entre 0 e 9 anos: </b> <?= $idadeChart['0_9'] ?> pessoa(s)<br>
                        <b>Entre 10 e 19 anos: </b> <?= $idadeChart['10_19'] ?> pessoa(s)<br>
                        <b>Entre 20 e 29 anos: </b> <?= $idadeChart['20_29'] ?> pessoa(s)<br>
                        <b>Entre 30 e 39 anos: </b> <?= $idadeChart['30_39'] ?> pessoa(s)<br>
                        <b>Mais de 40 anos: </b> <?= $idadeChart['40_'] ?> pessoa(s)<br>
                    </p>
                </div>
            </div>
        </div>
    </div>


</section>
<?= $this->Html->scriptStart(['block' => "scriptBottom"]); ?>
var sexoChart = new Chart( $("#sexoChart").get(0).getContext("2d") );
var idadeChart = new Chart( $("#idadeChart").get(0).getContext("2d") );

var sexoData = [
{
    value: <?= $sexoChart['indefinido'] ?>,
    color: "#3361cc",
    highlight: "#3361cc",
    label: "Não Declarado"
},
{
    value: <?= $sexoChart['masculino'] ?>,
    color: "#dc3912",
    highlight: "#dc3912",
    label: "Masculino"
},
{
    value: <?= $sexoChart['feminino'] ?>,
    color: "#ff9900",
    highlight: "#ff9900 ",
    label: "Feminino"
},
];


var idadeData = [
{
    value: <?= $idadeChart['0_9'] ?>,
    color: "#3361cc",
    highlight: "#3361cc",
    label: "De 0 à 9 anos"
},
{
    value: <?= $idadeChart['10_19'] ?>,
    color: "#dc3912",
    highlight: "#dc3912",
    label: "10 à 19 anos"
},
{
    value: <?= $idadeChart['20_29'] ?>,
    color: "#ff9900",
    highlight: "#ff9900",
    label: "20 à 29 anos"
},
{
    value: <?= $idadeChart['30_39'] ?>,
    color: "#109618",
    highlight: "#109618",
    label: "30 à 39 anos"
},
{
    value: <?= $idadeChart['40_'] ?>,
    color: "#990099",
    highlight: "#990099",
    label: "Mais de 40 anos"
},
];


var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50,
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
};

sexoChart.Pie(sexoData, pieOptions);
idadeChart.Pie(idadeData, pieOptions);
<?= $this->Html->scriptEnd(); ?>



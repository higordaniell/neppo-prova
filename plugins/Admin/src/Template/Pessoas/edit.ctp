<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= $subtitle ?> </h3>
    </div>

    <?= $this->Form->create($pessoa) ?>
    <div class="box-body">
        <div class="form-group">
            <?= $this->Form->input('identificao', [
                'class' => 'form-control',
                'placeholder' => 'Identificação da pessoa'
            ]) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->input('nome', ['class' =>
                'form-control',
                'placeholder' => 'Insira o nome da pessoa'
            ]) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->input('dataNascimento', [
                'class' => 'form-control',
                'monthNames' => false,
                'maxYear' => date('Y'),
                'minYear' => '1900',
                'label' => [
                    'style' => 'display: block',
                ],
                'year' => [
                    'class' => 'form-control',
                    'style' => 'display: inline; width: auto'
                ],
                'month' => [
                    'class' => 'form-control',
                    'style' => 'display: inline; width: auto'
                ],
                'day' => [
                    'class' => 'form-control',
                    'style' => 'display: inline; width: auto'
                ]
            ]) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->input('sexo', [
                'class' => 'form-control',
                'placeholder' => '',
                'options' => $sexos,
            ]) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->input('endereco', [
                'class' => 'form-control',
                'placeholder' => 'Endereço da Pessoa'
            ]) ?>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <?= $this->Form->button(__('Salvar'), ["class" => "btn btn-primary"]) ?>
    </div>
    <?= $this->Form->end() ?>
</div><!-- /.box -->

<?= $this->Html->script("//cdn.ckeditor.com/4.6.1/basic/ckeditor.js", ['block' => "scriptBottom"]); ?>
<?= $this->Html->scriptStart(['block' => "scriptBottom"]); ?>
$(function () {
});
<?= $this->Html->scriptEnd(); ?>
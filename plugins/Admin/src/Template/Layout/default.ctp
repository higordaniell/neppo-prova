<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?= $title ?> | Painel Infoalto </title>
    <?= $this->Html->charset() ?>
    <?= $this->Html->meta('viewport', 'width=device-width, initial-scale=1.0') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <?= $this->Html->css('AdminLTE.min.css') ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <?= $this->Html->css('skins/skin-blue.css') ?>
    <!-- iCheck -->
    <?= $this->Html->css('/plugins/iCheck/flat/blue.css') ?>
    <!-- jvectormap -->
    <?= $this->Html->css("/plugins/jvectormap/jquery-jvectormap-1.2.2.css"); ?>
    <!-- Date Picker -->
    <?= $this->Html->css("/plugins/datepicker/datepicker3.css"); ?>
    <!-- Daterange picker -->
    <?= $this->Html->css("/plugins/daterangepicker/daterangepicker-bs3.css"); ?>
    <!-- bootstrap wysihtml5 - text editor -->
    <?= $this->Html->css("/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?= $this->Html->css("https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"); ?>
    <?= $this->Html->css("https://oss.maxcdn.com/respond/1.4.2/respond.min.js"); ?>
    <![endif]-->

    <?= $this->fetch('script'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper skin-yellow-light">
    <?= $this->element('header'); ?>
    <?= $this->element('sidebar'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <?= $this->Flash->render() ?>
            <h1> <?= $title ?> <small><?= $subtitle ?></small></h1>
        </section>
        <section class="content" id="contentBody">
            <?= $this->fetch('content') ?>
        </section>
    </div>
    <?= $this->element('footer'); ?>
</div>

<?= $this->Html->script("/plugins/jQuery/jQuery-2.2.0.min.js"); ?>
<?= $this->Html->script("https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"); ?>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<?= $this->Html->script("bootstrap.min.js"); ?>
<?= $this->Html->script("/plugins/chartjs/Chart.min.js"); ?>

<!-- AdminLTE App -->
<?= $this->Html->script("app.min.js"); ?>
<?= $this->fetch('scriptBottom'); ?>
</body>
</html>

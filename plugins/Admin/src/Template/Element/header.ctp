<header class="main-header">
    <div class="logo">
        <span class='logo-mini'> <b>@</b> </span>
        <span class='logo-lg'> <b>Pessoas</b> </span>
    </div>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegação</span>
        </a>
    </nav>
</header>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?= $this->Menu->item('fa fa-dashboard', 'Gerenciar Pessoas', 'pessoas', 'index'); ?>
            <?= $this->Menu->item('fa fa-cog', 'Relatórios', 'relatorios', 'index'); ?>
        </ul>
    </section>
</aside>
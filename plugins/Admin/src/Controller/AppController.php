<?php
namespace Admin\Controller;
 
use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller {

    public $helpers = [
        'AdminTheme.Menu',
    ];

    public function initialize()
    {
        parent::initialize();

        $this->viewBuilder()->setTheme('AdminTheme');

        $this->loadComponent('Csrf');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }
}

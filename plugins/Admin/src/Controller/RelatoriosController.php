<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Relatorios Controller
 */
class RelatoriosController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('App.Pessoas');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
		$this->set('title', 'Relatorios');
		$this->set('subtitle', 'Visualizar relatorios');

		$sexoChart = [
		    'm' => $this->Pessoas->find()
                ->select(['count' => 'COUNT(*)'])
                ->where(['sexo' => 1])
                ->group('sexo')
                ->first(),

            'f' => $this->Pessoas->find()
                ->select(['count' => 'COUNT(*)'])
                ->where(['sexo' => 2])
                ->group('sexo')
                ->first(),

            'i' => $this->Pessoas->find()
                ->select(['count' => 'COUNT(*)'])
                ->where(['sexo' => 0])
                ->group('sexo')
                ->first()
        ];


        $sexoChart = $this->Pessoas->find('all')->select([
            'indefinido' => $this->Pessoas->find('all')->where(['sexo' => 0])->count('*'),
            'masculino' => $this->Pessoas->find('all')->where(['sexo' => 1])->count('*'),
            'feminino' => $this->Pessoas->find('all')->where(['sexo' => 2])->count('*'),
        ])->toArray()[0];

        $idadeChart = $this->Pessoas->find('all')->select([
            '0_9' => $this->Pessoas->find('all')
                ->where(['dataNascimento <= NOW()'])
                ->where(['dataNascimento > DATE_SUB(NOW(),INTERVAL 10 YEAR)'])
                ->count('*'),
            '10_19' => $this->Pessoas->find('all')
                ->where(['dataNascimento <= DATE_SUB(NOW(),INTERVAL 10 YEAR)'])
                ->where(['dataNascimento > DATE_SUB(NOW(),INTERVAL 20 YEAR)'])
                ->count('*'),
            '20_29' => $this->Pessoas->find('all')
                ->where(['dataNascimento <= DATE_SUB(NOW(),INTERVAL 20 YEAR)'])
                ->where(['dataNascimento > DATE_SUB(NOW(),INTERVAL 30 YEAR)'])
                ->count('*'),
            '30_39' => $this->Pessoas->find('all')
                ->where(['dataNascimento <= DATE_SUB(NOW(),INTERVAL 30 YEAR)'])
                ->where(['dataNascimento > DATE_SUB(NOW(),INTERVAL 40 YEAR)'])
                ->count('*'),
            '40_' => $this->Pessoas->find('all')
                ->where(['dataNascimento <= DATE_SUB(NOW(),INTERVAL 40 YEAR)'])
                ->count('*'),
        ])->toArray()[0];

		$this->set(compact('sexoChart', 'idadeChart'));
    }
}
